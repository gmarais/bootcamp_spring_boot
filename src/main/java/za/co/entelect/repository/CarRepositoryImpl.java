package za.co.entelect.repository;

import org.springframework.stereotype.Repository;
import za.co.entelect.domain.Car;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Stand JPA Repository
 */
@Repository
public class CarRepositoryImpl implements CarRepository{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Car car){
        entityManager.persist(car);
    }

    @Override
    public List<Car> listAll(){
        return (List<Car>) entityManager.createQuery("select c from Car c").getResultList();
    }
}