package za.co.entelect.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.entelect.domain.Photo;
import za.co.entelect.domain.User;

import java.util.List;

/**
 * Spring data jpa repository
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

    @Query("select pl from User u join u.photoList pl where u.id = :id")
    List<Photo> findUserPhotos(@Param("id") Long id);
}