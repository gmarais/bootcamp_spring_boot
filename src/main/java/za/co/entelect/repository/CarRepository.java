package za.co.entelect.repository;

import za.co.entelect.domain.Car;

import java.util.List;

public interface CarRepository {
    void save(Car car);
    List<Car> listAll();
}
