package za.co.entelect.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the core user of the system
 */
@Entity
@Table(name = "user")
public class User extends Identifiable {

    @Basic
    private String username;

    @Column(nullable = false, length = 254)
    private String emailAddress;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Photo> photoList = new ArrayList<>();

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }

    public void addPhoto(Photo photo) {
        photoList.add(photo);
    }
}
