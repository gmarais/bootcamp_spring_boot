package za.co.entelect.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;


/**
 * Domain object to model photos
 */
@Entity
public class Photo extends Identifiable {

    @Column(nullable = false, length = 100)
    private String title;

    @Column()
    private String description;

    @Lob
    @Column(length = 100000)
    private byte[] data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
