package za.co.entelect.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.co.entelect.domain.Photo;
import za.co.entelect.domain.User;
import za.co.entelect.service.UserService;

import java.util.List;

/**
 * Rest endpoint for user related data
 */

@RestController("/user")
public class UserController {

    @Autowired
    private UserService userService;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(method = RequestMethod.GET, path = "/id/{id}")
    public User getUserUsrById(@PathVariable Long id){
        LOG.info("Processing get user by with id = {}", id);
        return userService.fetch(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/name/{name}")
    public User getUserByUsername(@PathVariable String name){
        LOG.info("Processing get user by name with name = {}", name);
        return userService.findByUsername(name);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/photo/{id}")
    public List<Photo> getUserPhotosByUserId(@PathVariable Long id){
        LOG.info("Processing get user photos with user id = {}", id);
        return userService.findUserPhotos(id);
    }
}