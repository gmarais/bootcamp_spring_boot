package za.co.entelect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import za.co.entelect.domain.Photo;
import za.co.entelect.domain.User;
import za.co.entelect.service.UserServiceImpl;

import java.util.Arrays;

/*
 * Main class for application
 */
@SpringBootApplication
@ComponentScan({"za.co.entelect.controller", "za.co.entelect.service", "za.co.entelect.repository"})
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public CommandLineRunner printBeans(ApplicationContext context, UserServiceImpl userService) {
        return args -> {
            String[] beanNames = context.getBeanDefinitionNames();
            //print beans
            Arrays.sort(beanNames);
            for (String beanDefinitionName : beanNames) {
                LOG.debug("Bean created {}", beanDefinitionName);
            }


            User user1 = new User();
            user1.setUsername("Bob");
            user1.setEmailAddress("bob@gmail.com");

            Photo photoUser1 = new Photo();
            photoUser1.setTitle("Profile pic");
            photoUser1.setDescription("At HQ");
            user1.addPhoto(photoUser1);
            userService.save(user1);

            User user2 = new User();
            user2.setUsername("Sam");
            user2.setEmailAddress("sam@gmail.com");

            Photo photoUser2 = new Photo();
            photoUser2.setTitle("House");
            photoUser2.setDescription("Photo of house in JHB");
            user2.addPhoto(photoUser2);
            userService.save(user2);

            User user3 = new User();
            user3.setUsername("Joe");
            user3.setEmailAddress("joe@gmail.com");
            userService.save(user3);

            LOG.info("Sample data setup complete");
        };
    }
}