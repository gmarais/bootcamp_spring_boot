package za.co.entelect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.entelect.domain.Photo;
import za.co.entelect.domain.User;
import za.co.entelect.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User fetch(Long id){
        final Optional<User> userById = userRepository.findById(id);
        if(userById.isPresent()){
            return userById.get();
        }
        return null;
    }

    @Override
    public List<Photo> findUserPhotos(Long id) {
        return userRepository.findUserPhotos(id);
    }

    @Override
    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }
}