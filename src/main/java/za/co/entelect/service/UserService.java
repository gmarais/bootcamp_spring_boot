package za.co.entelect.service;

import za.co.entelect.domain.Photo;
import za.co.entelect.domain.User;

import java.util.List;

public interface UserService {
    void save(User user);
    User fetch(Long id);
    List<Photo> findUserPhotos(Long id);
    User findByUsername(String name);
}
