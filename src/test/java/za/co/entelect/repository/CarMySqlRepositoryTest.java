package za.co.entelect.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.entelect.DbConfig;
import za.co.entelect.domain.Car;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ActiveProfiles("mysql")
@Transactional
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = DbConfig.class)
public class CarMySqlRepositoryTest {

    @Autowired
    private CarRepository carRepository;

    @Test
    public void testList(){
        List<Car> cars = carRepository.listAll();
        assertNotNull(cars);
    }

}
