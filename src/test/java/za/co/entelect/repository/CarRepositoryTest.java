package za.co.entelect.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.entelect.domain.Car;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CarRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CarRepository carRepository;

    private Car car2 = new Car("Ford", "Focus", 355);
    private Car car1 = new Car("Opel", "Astra", 300);

    @Before
    public void initData(){
        testEntityManager.persist(car1);
        testEntityManager.persistAndFlush(car2);
    }

    @Test
    public void testList(){
        List<Car> cars = carRepository.listAll();
        assertNotNull(cars);
        assertEquals(2, cars.size());
    }

    @Test
    public void testSave(){
        Car bmw = new Car("BMW", "1M", 345);
        carRepository.save(bmw);

        Car car = testEntityManager.find(Car.class, bmw.getId());
        assertNotNull(car);
    }
}