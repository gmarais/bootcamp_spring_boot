package za.co.entelect.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.entelect.domain.Photo;
import za.co.entelect.domain.User;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserService userService;

    private static final String USER_1 = "Heidi";
    private User user;

    @Before
    public void setup() {
        user = createTestUser();
        testEntityManager.persist(user);
    }   

    @Test
    public void testGetById() {
        assertNotNull(userService.fetch(user.getId()));
    }

    @Test
    public void testGetUserByUsername() {
        User heidi = userService.findByUsername(USER_1);
        assertNotNull(heidi);
        assertEquals(USER_1, heidi.getUsername());
        assertEquals(2, user.getPhotoList().size());
    }

    @Test
    public void testFindPhotosByUserId() {
        List<Photo> userPhotos = userService.findUserPhotos(user.getId());

        assertNotNull(userPhotos);
        assertEquals(2, userPhotos.size());
        Photo photo1 = userPhotos.get(0);
        assertNotNull(photo1.getTitle());
        assertNotNull(photo1.getDescription());
        assertEquals(photo1.getTitle(), photo1.getDescription());
    }

    private User createTestUser() {
        User user = new User();
        user.setUsername(USER_1);
        user.setEmailAddress("heidi@gmail.com");
        addPhoto(user, "Office pic");
        addPhoto(user, "Home pic");
        return user;
    }

    private void addPhoto(User user, String s) {
        Photo photo1 = new Photo();
        photo1.setTitle(s);
        photo1.setDescription(s);
        user.addPhoto(photo1);
    }
}