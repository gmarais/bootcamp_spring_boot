package za.co.entelect;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import za.co.entelect.repository.CarRepository;
import za.co.entelect.repository.CarRepositoryImpl;
import za.co.entelect.repository.UserRepository;
import za.co.entelect.service.UserService;
import za.co.entelect.service.UserServiceImpl;

@Configuration
@ComponentScan("za.co.entelect.controller")
public class TestApplicationContext {

    @Bean
    public UserRepository userRepository() {
        return Mockito.mock(UserRepository.class);
    }

    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

}