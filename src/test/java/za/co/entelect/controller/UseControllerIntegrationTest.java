package za.co.entelect.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.entelect.TestApplicationContext;
import za.co.entelect.repository.UserRepository;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Additional test to demonstrate setting up a test context
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes =  TestApplicationContext.class)
public class UseControllerIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserController userController;

    @Test
    public void testFetch(){
        userController.getUserUsrById(1L);
        verify(userRepository).findById(eq(1L));
    }
}
