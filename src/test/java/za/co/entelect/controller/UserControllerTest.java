package za.co.entelect.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import za.co.entelect.domain.User;
import za.co.entelect.service.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    private static final String TEST_USERNAME = "Max";

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @Test
    public void testGetUserById(){
        userController.getUserUsrById(1L);
        verify(userService).fetch(eq(1L));
    }

    @Test
    public void testGetPhotoByUserId(){
        userController.getUserPhotosByUserId(1L);
        verify(userService).findUserPhotos(eq(1L));
    }

    @Test
    public void testGetUserByUserName(){
        when(userService.findByUsername(anyString())).thenReturn(createTestUser(TEST_USERNAME));

        User user = userController.getUserByUsername("user");

        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getUsername());
        verify(userService).findByUsername(anyString());
    }

    private User createTestUser(String username) {
        User user = new User();
        user.setUsername(username);
        return user;
    }
}