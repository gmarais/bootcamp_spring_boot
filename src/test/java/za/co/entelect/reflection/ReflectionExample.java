package za.co.entelect.reflection;

import org.junit.Test;
import za.co.entelect.domain.Car;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ReflectionExample {

    private static final String MAKE = "lamborghini";
    private static final int HORSEPOWER = 740;

    private Car lambo = new Car(MAKE, "veneno", HORSEPOWER);

    @Test
    public void testCreateInstance() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Object testCar = Car.class.getConstructors()[0].newInstance("Honda", "Civic", 306);

        System.out.println(testCar);
        assertTrue(testCar instanceof Car);
    }


    @Test
    public void testFieldReflection() throws NoSuchFieldException, IllegalAccessException {
        Class<Car> carClass = Car.class;
        Field makeField = carClass.getDeclaredField("make");
        makeField.setAccessible(true);

        String make = (String) makeField.get(lambo);

        System.out.println("Make=" + make);

        assertEquals(MAKE, make);
    }

    @Test
    public void testMethodRefection() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getHorsepower = Car.class.getMethod("getHorsepower");

        Integer power = (Integer) getHorsepower.invoke(lambo);

        System.out.println("Horsepower=" + power);

        assertEquals(HORSEPOWER, power.intValue());
    }

}
